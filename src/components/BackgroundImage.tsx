import React, {Component} from 'react';
import {StyleSheet, Image} from 'react-native';
import { background } from '../assets/background';

class BackgroundImage extends Component {
  render() {
    return (
      <Image
        style={styles.backgroundImage}
        source={background}
      />
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'stretch',
    position: 'absolute', 
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    opacity: 0.6
  },
});

export default BackgroundImage;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';

import { Button, Text, Badge } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';

interface PropsInterface {
  sendButton: any;
  badge: number;
  icon: string;
  text: string;
  index: number;
  positionIndex: number;
}

class TabComponent extends Component<PropsInterface> {
  onEventsendButton(value: any) {
    if (this.props.sendButton) {
      this.props.sendButton({ indexPage: value });
    }
  }

  navigateToAction(value: any) {
    Actions.push(value);
  }

  renderBadge() {
    if (this.props.badge > 0) {
      return (
        <Badge style={{ alignContent: 'center', alignItems: 'center', marginLeft: 60 }}>
          <Text style={{ color: 'white' }}>{this.props.badge}</Text>
        </Badge>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <Button
        onPress={() => this.onEventsendButton(this.props.index)}
        style={{ marginTop: this.props.badge > 0 ? 0 : 15 }}
        vertical
      >
        {this.renderBadge()}
        <Icon name={this.props.icon} />
        <Text
          style={{
            color:
              this.props.positionIndex === this.props.index ? 'red' : 'black'
          }}
        >
          {this.props.text}
        </Text>
      </Button>
    );
  }
}

export default TabComponent;

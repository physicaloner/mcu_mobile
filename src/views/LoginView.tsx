/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {
  Container,
  Header,
  Content,
  Input,
  Item,
  Button,
  Text,
  Thumbnail,
  Toast,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Actions} from 'react-native-router-flux';
import BackgroundImage from '../components/BackgroundImage';
import {square_logo} from '../assets/background';
import {ENVIRONMENT} from '../environment';
import {AsyncStorage} from 'react-native';
import {LoginModel} from '../models/login.model';

interface State {}

interface Props {}

class LoginView extends Component<Props, State> {
  username = '';
  password = '';

  navigateToMaster() {
    Actions.replace('master');
  }

  navigateToRegister() {
    Actions.register();
  }

  getLogin() {
    fetch(
      `${ENVIRONMENT.apiLink}/api/USER_INFO/${this.username}/${this.password}`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then(t => t.json())
      .then((value: LoginModel) => {
        if (value.USRID === this.username && value.USRPWD === this.password) {
          AsyncStorage.removeItem('user_cache');
          AsyncStorage.setItem('user_cache', JSON.stringify(value));
          Toast.show({
            text: 'Login Success.',
            buttonText: 'Okay',
            duration: 3000,
          });
          this.navigateToMaster();
        } else {
          Toast.show({
            text: 'User name or password invalid.',
            buttonText: 'Okay',
            duration: 3000,
          });
        }
      })
      .catch(error => {
        Toast.show({
          text: 'Error : Network is invalid.',
          buttonText: 'Okay',
          duration: 3000,
        });
        // this.navigateToMaster();
      });
  }

  postLogin() {
    fetch(`${ENVIRONMENT.apiLink}/api`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstParam: 'yourValue',
        secondParam: 'yourOtherValue',
      }),
    });
  }

  render() {
    return (
      <Container>
        <Content
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <BackgroundImage />
          <Thumbnail
            square
            source={square_logo}
            style={{width: 150, height: 150}}
          />
          <Item rounded style={{marginTop: 25, width: '75%'}}>
            <Icon name="user" style={{marginLeft: 20}} />
            <Input
              placeholder="User Name"
              style={{color: 'black'}}
              placeholderTextColor="black"
              onChangeText={username => (this.username = username)}
            />
          </Item>
          <Item rounded style={{marginTop: 15, width: '75%'}}>
            <Icon name="user" style={{marginLeft: 20}} />
            <Input
              placeholder="Password"
              style={{color: 'black'}}
              placeholderTextColor="black"
              onChangeText={password => (this.password = password)}
              secureTextEntry={true}
            />
          </Item>
          <Button
            primary
            rounded
            onPress={() => this.getLogin()}
            style={{marginTop: 60, alignItems: 'center', width: '50%'}}>
            <Text style={{width: '100%', textAlign: 'center'}}>Login</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default LoginView;

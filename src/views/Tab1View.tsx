/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {
  Container,
  Header,
  Body,
  Content,
  Toast,
  Button,
  Text,
  Thumbnail,
} from 'native-base';
// import { Actions } from 'react-native-router-flux';
import Voice from 'react-native-voice';
import BackgroundImage from '../components/BackgroundImage';
import {AsyncStorage} from 'react-native';
import {ENVIRONMENT} from '../environment';
import {LoginModel} from 'models/login.model';

interface State {
  recognized: string;
  pitch: string;
  error: string;
  end: string;
  started: string;
  results: Array<string>;
  partialResults: Array<string>;
  isListen: boolean;
}

interface Props {}

class Tab1View extends Component<Props, State> {
  state: State = {
    recognized: '',
    pitch: '',
    error: '',
    end: '',
    started: '',
    results: [],
    partialResults: [],
    isListen: false,
  };
  modelData!: LoginModel;

  constructor(props: Props) {
    super(props);
    Voice.onSpeechStart = this.onSpeechStart;
    Voice.onSpeechRecognized = this.onSpeechRecognized;
    Voice.onSpeechEnd = this.onSpeechEnd;
    Voice.onSpeechError = this.onSpeechError;
    Voice.onSpeechResults = this.onSpeechResults;
    Voice.onSpeechPartialResults = this.onSpeechPartialResults;
    Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged;
  }

  async componentWillMount() {
    try {
      const storage: any = await AsyncStorage.getItem('user_cache');
      this.modelData = JSON.parse(storage);
    } catch {
      Toast.show({
        text: 'Error : You are not login !',
        buttonText: 'Okay',
        duration: 3000,
      });
    }
  }

  componentWillUnmount() {
    Voice.destroy().then(Voice.removeAllListeners);
  }

  setDoorOn() {
    fetch(`${ENVIRONMENT.mcuLink}/on`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(t => t.json())
      .then(value => {
        Toast.show({
          text: 'Door On.',
          buttonText: 'Okay',
          duration: 3000,
        });
      })
      .catch(error => {
        Toast.show({
          text: 'Error : network error.',
          buttonText: 'Okay',
          duration: 3000,
        });
      });
  }

  setDoorOff() {
    fetch(`${ENVIRONMENT.mcuLink}/off`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(t => t.json())
      .then(value => {
        Toast.show({
          text: 'Door Off.',
          buttonText: 'Okay',
          duration: 3000,
        });
      })
      .catch(error => {
        Toast.show({
          text: 'Error : network error.',
          buttonText: 'Okay',
          duration: 3000,
        });
      });
  }

  onSpeechStart = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechStart: ', e);
    this.state.started = '√';
  };

  onSpeechRecognized = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechRecognized: ', e);
    this.state.recognized = '√';
  };

  onSpeechEnd = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechEnd: ', e);
    this.setState({
      end: '√',
    });
    this.state.isListen = false;
  };

  onSpeechError = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechError: ', e);
    this.setState({
      error: JSON.stringify(e.error),
    });
    Toast.show({
      text: 'Error : ' + e.error.message,
      buttonText: 'Okay',
      duration: 3000,
    });
    this.state.isListen = false;
    // Voice.stop();
  };

  onSpeechResults = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechResults: ', e);
    this.setState({
      results: e.value,
    });
    if (this.modelData) {
      const say: string = e.value[0];
      if (say.toLowerCase() === this.modelData.KEY_WORD_ON.toLowerCase()) {
        this.setDoorOn();
      } else if (say.toLowerCase() === this.modelData.KEY_WORD_OFF.toLowerCase()) {
        this.setDoorOff();
      } else {
        Toast.show({
          text: 'Not match case in >> ' + e.value[0],
          buttonText: 'Okay',
          duration: 3000,
        });
      }
    }
    this.state.isListen = false;
  };

  onSpeechPartialResults = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechPartialResults: ', e);
    this.setState({
      partialResults: e.value,
    });
  };

  onSpeechVolumeChanged = (e: any) => {
    // eslint-disable-next-line
    console.log('onSpeechVolumeChanged: ', e);
    this.setState({
      pitch: e.value,
    });
  };

  _startRecognizing = async () => {
    this.setState({
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });

    try {
      this.state.isListen = true;
      await Voice.start('en-US');
    } catch (e) {
      //eslint-disable-next-line
      this.state.isListen = false;
      console.error(e);
    }
  };

  _stopRecognizing = async () => {
    try {
      this.state.isListen = false;
      await Voice.stop();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };

  _cancelRecognizing = async () => {
    try {
      this.state.isListen = false;
      await Voice.cancel();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };

  _destroyRecognizer = async () => {
    try {
      this.state.isListen = false;
      await Voice.destroy();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
    this.setState({
      recognized: '',
      pitch: '',
      error: '',
      started: '',
      results: [],
      partialResults: [],
      end: '',
    });
  };

  sendMshTo(msg: string) {
    fetch('', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        word: msg,
        success: true,
      }),
    })
      .then(t => t.json())
      .then(t => {
        return t;
      });
  }

  renderStart() {
    if (!this.state.isListen) {
      return (
        <Button
          primary
          rounded
          onPress={this._startRecognizing.bind(this)}
          style={{
            marginTop: 60,
            alignItems: 'center',
            width: '50%',
            backgroundColor: 'green',
          }}>
          <Text style={{width: '100%', textAlign: 'center'}}>RECORD</Text>
        </Button>
      );
    } else {
      return (
        <Button
          primary
          rounded
          onPress={this._stopRecognizing.bind(this)}
          style={{
            marginTop: 60,
            alignItems: 'center',
            width: '50%',
            backgroundColor: 'yellow',
          }}>
          <Text style={{width: '100%', textAlign: 'center', color: 'black'}}>
            Listen...
          </Text>
        </Button>
      );
    }
  }

  render() {
    return (
      <Container>
        <Header hasTabs>
          <Body style={{alignContent: 'center'}}>
            <Text style={{color: '#ffffff'}}>Audio</Text>
          </Body>
        </Header>
        <Content
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <BackgroundImage />
          <Thumbnail
            square
            source={{
              uri:
                'https://raw.githubusercontent.com/react-native-community/react-native-voice/master/VoiceTest/src/button.png',
            }}
            style={{backgroundColor: 'transparent', width: 150, height: 150}}
          />

          <Text style={{marginTop: 20, textAlign: 'center'}}>
            {'Last word : ' +
              JSON.stringify(
                this.state.results[0] === undefined
                  ? ''
                  : this.state.results[0],
              )}
          </Text>
          {this.renderStart()}
        </Content>
      </Container>
    );
  }
}

export default Tab1View;

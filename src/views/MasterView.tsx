import React, {Component} from 'react';
import {Container, Footer, FooterTab, Toast} from 'native-base';

import TabComponent from '../components/TabComponent';
import Tab1View from './Tab1View';
import Tab2View from './Tab2View';
import Tab3View from './Tab3View';
import {AsyncStorage} from 'react-native';
import {LoginModel} from 'models/login.model';

interface State {
  indexPage: number;
}

interface Props {}

class MasterView extends Component<Props, State> {
  state: State = {indexPage: 1};

  sendButton(value: any) {
    this.setState(value);
  }

  render() {
    return (
      <Container>
        {this.renderContent()}
        <Footer style={{backgroundColor: 'white'}}>
          <FooterTab style={{backgroundColor: 'white'}}>
            <TabComponent
              text="Audio"
              icon="microphone"
              badge={0}
              index={1}
              positionIndex={this.state.indexPage}
              sendButton={this.sendButton.bind(this)}
            />
            <TabComponent
              text="Control"
              icon="grip-horizontal"
              badge={0}
              index={2}
              positionIndex={this.state.indexPage}
              sendButton={this.sendButton.bind(this)}
            />
            <TabComponent
              text="Setting"
              icon="cog"
              badge={0}
              index={3}
              positionIndex={this.state.indexPage}
              sendButton={this.sendButton.bind(this)}
            />
          </FooterTab>
        </Footer>
      </Container>
    );
  }

  renderContent() {
    switch (this.state.indexPage) {
      case 1: {
        return <Tab1View />;
      }
      case 2: {
        return <Tab2View />;
      }
      case 3: {
        AsyncStorage.getItem('user_cache')
          .then(store => {
            if (store !== null) {
              const modelData: LoginModel = JSON.parse(String(store));
              return (
                <Tab3View
                  model={modelData}
                  speechon={modelData.KEY_WORD_ON}
                  speechoff={modelData.KEY_WORD_OFF}
                />
              );
            } else {
              this.state.indexPage = 1;
              Toast.show({
                text: 'Store is invalid !',
                buttonText: 'Okay',
                duration: 3000,
              });
            }
          })
          .catch(ex => {
            this.state.indexPage = 1;
            Toast.show({
              text: 'Error : You are not login !',
              buttonText: 'Okay',
              duration: 3000,
            });
          });
      }
      case 4: {
        return null;
      }
    }
  }
}

export default MasterView;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {
  Container,
  Header,
  Body,
  Content,
  Input,
  Item,
  Button,
  Text,
  Thumbnail,
  Toast,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Actions} from 'react-native-router-flux';
import BackgroundImage from '../components/BackgroundImage';
import {AsyncStorage} from 'react-native';
import {ENVIRONMENT} from '../environment';
import {LoginModel} from '../models/login.model';

interface State {}

interface Props {}

class Tab2View extends Component<Props, State> {
  setDoorOn() {
    fetch(`${ENVIRONMENT.mcuLink}/on`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(t => t.json())
      .then(value => {
        Toast.show({
          text: 'Door On.',
          buttonText: 'Okay',
          duration: 3000,
        });
      })
      .catch(error => {
        Toast.show({
          text: 'Error : network error.',
          buttonText: 'Okay',
          duration: 3000,
        });
      });
  }

  setDoorOff() {
    fetch(`${ENVIRONMENT.mcuLink}/off`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(t => t.json())
      .then(value => {
        Toast.show({
          text: 'Door Off.',
          buttonText: 'Okay',
          duration: 3000,
        });
      })
      .catch(error => {
        Toast.show({
          text: 'Error : network error.',
          buttonText: 'Okay',
          duration: 3000,
        });
      });
  }

  render() {
    return (
      <Container>
        <Header hasTabs>
          <Body style={{alignContent: 'center'}}>
            <Text style={{color: '#ffffff'}}>Manual & Control</Text>
          </Body>
        </Header>
        <Content
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <BackgroundImage />
          <Button
            primary
            rounded
            style={{
              alignItems: 'center',
              width: '50%',
              backgroundColor: 'green',
            }}
            onPress={() => this.setDoorOn()}>
            <Text style={{width: '100%', textAlign: 'center'}}>ON</Text>
          </Button>
          <Button
            primary
            rounded
            style={{
              marginTop: 60,
              alignItems: 'center',
              width: '50%',
              backgroundColor: 'red',
            }}
            onPress={() => this.setDoorOff()}>
            <Text style={{width: '100%', textAlign: 'center'}}>OFF</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Tab2View;

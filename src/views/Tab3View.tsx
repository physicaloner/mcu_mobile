/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {
  Container,
  Header,
  Body,
  Content,
  Input,
  Item,
  Button,
  Text,
  Thumbnail,
  Toast,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome5';
import BackgroundImage from '../components/BackgroundImage';
import {AsyncStorage} from 'react-native';
import {LoginModel} from '../models/login.model';
import {ENVIRONMENT} from '../environment';

interface State {
  speechon: string;
  speechoff: string;
}

interface Props {
  model: LoginModel;
  speechon: string;
  speechoff: string;
}

class Tab3View extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.modelData = props.model;
    this.speechon = props.speechon;
    this.speechoff = props.speechoff;
  }

  updateValue() {
    try {
      if (this.speechon === this.speechoff) {
        Toast.show({
          text: 'Cannot use duplicate word.',
          buttonText: 'Okay',
          duration: 3000,
        });
        return;
      }
      this.modelData.KEY_WORD_ON = this.speechon;
      this.modelData.KEY_WORD_OFF = this.speechoff;
      AsyncStorage.removeItem('user_cache');
      AsyncStorage.setItem('user_cache', JSON.stringify(this.modelData));
      fetch(`${ENVIRONMENT.apiLink}/api/USER_INFO/${this.modelData.ID}`, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.modelData),
      })
        .then(t => t.json())
        .then(value => {
          Toast.show({
            text: 'Update success.',
            buttonText: 'Okay',
            duration: 3000,
          });
        })
        .catch(error => {
          Toast.show({
            text: 'Update success (No data).',
            buttonText: 'Okay',
            duration: 3000,
          });
        });
    } catch {
      Toast.show({
        text: 'Error : Not found user login.',
        buttonText: 'Okay',
        duration: 3000,
      });
    }
  }

  modelData: LoginModel;
  speechon: string;
  speechoff: string;

  render() {
    return (
      <Container>
        <Header hasTabs>
          <Body style={{alignContent: 'center'}}>
            <Text style={{color: '#ffffff'}}>Setting</Text>
          </Body>
        </Header>
        <Content
          contentContainerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <BackgroundImage />
          <Item rounded style={{marginTop: 25, width: '75%'}}>
            <Icon name="user" style={{marginLeft: 20}} />
            <Input
              placeholder="On Message"
              style={{color: 'black'}}
              placeholderTextColor="black"
              onChangeText={onmsg => (this.speechon = onmsg)}
              defaultValue={this.speechon}
            />
          </Item>
          <Item rounded style={{marginTop: 15, width: '75%'}}>
            <Icon name="user" style={{marginLeft: 20}} />
            <Input
              placeholder="Off Message"
              style={{color: 'black'}}
              placeholderTextColor="black"
              onChangeText={offmsg => (this.speechoff = offmsg)}
              defaultValue={this.speechoff}
            />
          </Item>
          <Button
            primary
            rounded
            style={{
              alignItems: 'center',
              width: '50%',
              backgroundColor: 'blue',
              marginTop: 60,
            }}
            onPress={() => this.updateValue()}>
            <Text style={{width: '100%', textAlign: 'center', color: 'white'}}>
              Update Speech
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Tab3View;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import LoginView from './views/LoginView';
import MasterView from './views/MasterView';
import {Root} from 'native-base';
import {Router, Stack, Scene} from 'react-native-router-flux';
import Tab1View from './views/Tab1View';
import Tab2View from './views/Tab2View';
import Tab3View from './views/Tab3View';

class RouterComponent extends React.Component {
  render() {
    return (
      <Root>
        <Router>
          <Stack key="root">
            <Scene key="login" component={LoginView} title="Login" hideNavBar />
            <Scene
              key="master"
              component={MasterView}
              title="Master"
              hideNavBar
            />
            <Scene key="tab1" component={Tab1View} title="Tab1" hideNavBar />
            <Scene key="tab2" component={Tab2View} title="Tab2" hideNavBar />
            <Scene key="tab3" component={Tab3View} title="Tab3" hideNavBar />
          </Stack>
        </Router>
      </Root>
    );
  }
}

export default RouterComponent;

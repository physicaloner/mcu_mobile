export interface LoginModel {
  ID: number;
  USRID: string;
  USRPWD: string;
  KEY_WORD_ON: string;
  KEY_WORD_OFF: string;
}

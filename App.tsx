/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {Component} from 'react';
import {BackHandler} from 'react-native';
import {Actions} from 'react-native-router-flux';
import RouterComponent from './src/Router';

class App extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => Actions.pop());
  }

  render() {
    return <RouterComponent />;
  }
}

export default App;
